package com.epione.timeline.services;

import com.epione.timeline.domain.model.types.SingleDateTimeTree;

import java.time.LocalDateTime;

public interface AddNodesToGraphService {

    SingleDateTimeTree prepareNodesFromDate(LocalDateTime dateTime);

}
