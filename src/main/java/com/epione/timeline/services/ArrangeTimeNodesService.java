package com.epione.timeline.services;

import com.epione.timeline.domain.model.types.SingleDateTimeTree;


public interface ArrangeTimeNodesService {

//    void arrangeNodesLogic(LocalDateTime dateTime, SingleDateTimeTree singleDateTimeTree);

    void addToNextTree(Long dateTimeEpoch, SingleDateTimeTree addedTree);

    void addToPreviousTree(Long dateTimeEpoch, SingleDateTimeTree addedTree);

}
