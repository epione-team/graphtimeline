package com.epione.timeline.services.impl;

import com.epione.timeline.domain.model.TimeNode;
import com.epione.timeline.domain.model.types.SingleDateTimeTree;
import com.epione.timeline.services.AddNodesToGraphService;
import com.epione.timeline.services.ArrangeTimeNodesService;
import com.epione.timeline.services.FindDateInGraphService;
import com.epione.timeline.services.GraphTimelineService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;

@Service
@Slf4j
public class GraphTimelineServiceImpl implements GraphTimelineService {

    @Autowired
    private AddNodesToGraphService addNodesToGraphService;

    @Autowired
    private ArrangeTimeNodesService arrangeTimeNodesService;

    @Autowired
    private FindDateInGraphService findDateInGraphService;

    @Override
    public TimeNode addSingleDateToGraph(LocalDateTime dateTime) {
        Long dateTimeEpoch = dateTime.atZone(ZoneId.systemDefault()).toEpochSecond();
        SingleDateTimeTree addedTree = addNodesToGraphService.prepareNodesFromDate(dateTime);
        arrangeTimeNodesService.addToNextTree(dateTimeEpoch, addedTree);
        arrangeTimeNodesService.addToPreviousTree(dateTimeEpoch, addedTree);
        log.info("Added: {}", dateTime);
        return addedTree.getHour();
    }

    @Override
    public TimeNode getTimeNodeFromDate(LocalDateTime dateTime) {
        Long dateTimeEpoch = dateTime.atZone(ZoneId.systemDefault()).toEpochSecond();
        Optional<TimeNode> optionalTimeNode = findDateInGraphService.findDate(dateTimeEpoch);
        if(optionalTimeNode.isPresent()) {
            return optionalTimeNode.get();
        } else {
            return addSingleDateToGraph(dateTime);
        }
    }
}
