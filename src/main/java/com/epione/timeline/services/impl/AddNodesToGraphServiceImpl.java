package com.epione.timeline.services.impl;

import com.epione.timeline.domain.model.*;
import com.epione.timeline.domain.model.types.SingleDateTimeTree;
import com.epione.timeline.domain.repository.*;
import com.epione.timeline.services.AddNodesToGraphService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;
import java.util.UUID;

import static java.time.temporal.TemporalAdjusters.lastDayOfMonth;

@Slf4j
@Service
public class AddNodesToGraphServiceImpl implements AddNodesToGraphService {

    @Autowired
    private YearRepository yearRepo;

    @Autowired
    private MonthRepository monthRepo;

    @Autowired
    private DayRepository dayRepo;

    @Autowired
    private HourRepository hourRepo;

    @Autowired
    private MinuteRepository minuteRepo;

    @Override
    @Transactional
    public SingleDateTimeTree prepareNodesFromDate(LocalDateTime dateTime) {
        Year year = addOrGetYear(dateTime);
        Month month = addOrGetMonth(dateTime);
        Day day = addOrGetDay(dateTime);
        Hour hour = addOrGetHour(dateTime);
        Minute minute = addOrGetMinute(dateTime);
        SingleDateTimeTree singleDateTimeTree = new SingleDateTimeTree(year, month, day, hour, minute);
        setFirstAndLastLogic(dateTime, singleDateTimeTree);
        saveTree(singleDateTimeTree);
        return singleDateTimeTree;
    }

    private Year addOrGetYear(LocalDateTime dt) {
        log.info("Checking year value: {}", dt.getYear());
        Optional<Year> yearOptional = yearRepo.findByValue(dt.getYear());
        return yearOptional.orElseGet(() -> {
            log.info("Creating a new Year: {}", dt.getYear());
            Year year = new Year(dt.getYear());
            year.setUuid(UUID.randomUUID().toString());
            return yearRepo.save(year);
        });

    }

    private Month addOrGetMonth(LocalDateTime dt) {
        Month month;
        Optional<Month> monthOptional = monthRepo.getMonthRelatedToYear(dt.getMonthValue(), dt.getYear());
        if (monthOptional.isPresent()) {
            month = monthRepo.findById(monthOptional.get().getId()).get();
        } else {
            log.info("Creating a new Month: {}", dt.getMonthValue());
            month = new Month(dt.getMonthValue());
            month.setUuid(UUID.randomUUID().toString());
            month = monthRepo.save(month);
        }
        log.info("Return Month: {}", month);
        return month;
    }

    private Day addOrGetDay(LocalDateTime dt) {
        Day day;
        Optional<Day> dayOptional = dayRepo.findByValueRelatedToYearAndMonth(dt.getDayOfMonth(), dt.getMonthValue(), dt.getYear());
        if (dayOptional.isPresent()) {
            day = dayRepo.findById(dayOptional.get().getId()).get();
        } else {
            log.info("Creating a new Day:{}", dt.getDayOfMonth());
            day = new Day(dt.getDayOfMonth());
            day.setUuid(UUID.randomUUID().toString());
            day = dayRepo.save(day);
        }
        log.info("Return Day: {}", day);
        return day;
    }

    private Hour addOrGetHour(LocalDateTime dt) {
        Hour hour;
        Optional<Hour> hourOptional = hourRepo.findByValueRelatedToYearMonthAndDay(dt.getHour(), dt.getDayOfMonth(),
                dt.getMonthValue(), dt.getYear());
        if (hourOptional.isPresent()) {
            hour = hourRepo.findById(hourOptional.get().getId()).get();
        } else {
            log.info("Creating a new Hour: {}", dt.getHour());
            hour = new Hour(dt.getHour());
            hour.setUuid(UUID.randomUUID().toString());
//            hour.setDateTime(dt.atZone(ZoneId.systemDefault()).toEpochSecond());
            return hourRepo.save(hour);
        }
        log.info("Return Hour: {}", hour);
        return hour;
    }

    private Minute addOrGetMinute(LocalDateTime dt) {
        Minute minute;
        Optional<Minute> minuteOptional = minuteRepo.findByValueRelatedToYearMonthDayAndHour(dt.getMinute(), dt.getHour(), dt.getDayOfMonth(),
                dt.getMonthValue(), dt.getYear());
        if (minuteOptional.isPresent()) {
            minute = minuteRepo.findById(minuteOptional.get().getId()).get();
        } else {
            log.info("Creating new minute: {} from: {}", dt.getMinute(), dt.toString());
            minute = new Minute(dt.getMinute());
            minute.setUuid(UUID.randomUUID().toString());
            minute.setDateTime(dt.atZone(ZoneId.systemDefault()).toEpochSecond());
        }
        return minute;
    }

    private void setFirstAndLastLogic(LocalDateTime dateTime, SingleDateTimeTree addedTree) {
        addedTree.getMonth().setFirst(dateTime.getMonthValue() == 1);
        addedTree.getMonth().setLast(dateTime.getMonthValue() == 12);

        addedTree.getDay().setFirst(dateTime.getDayOfMonth() == 1);
        addedTree.getDay().setLast(dateTime.equals(dateTime.with(lastDayOfMonth())));

        addedTree.getHour().setFirst(dateTime.getHour() == 0);
        addedTree.getHour().setLast(dateTime.getHour() == 23);

        addedTree.getMinute().setFirst(dateTime.getMinute() == 0);
        addedTree.getMinute().setLast(dateTime.getMinute() == 59);
    }

    private void saveTree(SingleDateTimeTree singleDateTimeTree) {
        singleDateTimeTree.setYear(yearRepo.save(singleDateTimeTree.getYear()));
        singleDateTimeTree.setMonth(monthRepo.save(singleDateTimeTree.getMonth()));
        singleDateTimeTree.setDay(dayRepo.save(singleDateTimeTree.getDay()));
        singleDateTimeTree.setHour(hourRepo.save(singleDateTimeTree.getHour()));
        singleDateTimeTree.setMinute(minuteRepo.save(singleDateTimeTree.getMinute()));
    }
}
