package com.epione.timeline.services.impl;

import com.epione.timeline.domain.model.Hour;
import com.epione.timeline.domain.model.TimeNode;
import com.epione.timeline.domain.repository.TimeNodeRepository;
import com.epione.timeline.services.FindDateInGraphService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Slf4j
public class FindDateInGraphServiceImpl implements FindDateInGraphService {

    @Autowired
    private TimeNodeRepository timeNodeRepo;

    @Override
    @Transactional(readOnly = true)
    public Optional<TimeNode> findDate(Long dateTimeEpoch) {
        return timeNodeRepo.findByDateTime(dateTimeEpoch);
    }
}
