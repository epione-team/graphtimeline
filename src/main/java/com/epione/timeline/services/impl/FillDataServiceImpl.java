package com.epione.timeline.services.impl;

import com.epione.timeline.services.FillDataService;
import com.epione.timeline.services.GraphTimelineService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;

import static java.time.temporal.TemporalAdjusters.lastDayOfMonth;

@Service
@Slf4j
public class FillDataServiceImpl implements FillDataService {

    private final LocalDateTime BEGIN_TIME = LocalDateTime.of(2012, java.time.Month.JANUARY, 1, 0, 0);

    //    private final LocalDateTime END_TIME = LocalDateTime.of(2200, Month.JANUARY, 1, 0, 0);
    private final LocalDateTime END_TIME = LocalDateTime.of(2018, Month.FEBRUARY, 1, 0, 0);

    @Autowired
    private GraphTimelineService graphTimelineService;

    @Override
//    @PostConstruct
    public void fillDbWithTimeline() {
        log.info("Beginning filling the DB");
//        addConstantDates();
//        addRandomDates(20);

    }

    private void addConstantDates() {
        System.out.println(graphTimelineService.addSingleDateToGraph(LocalDateTime.of(2017, 12, 31, 18, 0)));
        System.out.println(graphTimelineService.addSingleDateToGraph(LocalDateTime.of(2017, 12, 31, 18, 0)));
        System.out.println(graphTimelineService.addSingleDateToGraph(LocalDateTime.of(2018, 2, 2, 19, 0)));
        System.out.println(graphTimelineService.addSingleDateToGraph(LocalDateTime.of(2017, 8, 10, 17, 0)));
        System.out.println(graphTimelineService.addSingleDateToGraph(LocalDateTime.of(2017, 8, 12, 17, 0)));
        System.out.println(graphTimelineService.addSingleDateToGraph(LocalDateTime.of(2017, 8, 10, 10, 0)));
        System.out.println(graphTimelineService.addSingleDateToGraph(LocalDateTime.of(2017, 8, 11, 12, 0)));
    }

    private void addRandomDates(int numOfDates) {
        long minDay = BEGIN_TIME.atZone(ZoneId.systemDefault()).toEpochSecond();
        long maxDay = END_TIME.atZone(ZoneId.systemDefault()).toEpochSecond();


        for (int i = 0; i < numOfDates; i++) {
            long random = minDay + (long) (Math.random() * (maxDay - minDay));
            graphTimelineService.addSingleDateToGraph(LocalDateTime.ofInstant(Instant.ofEpochSecond(random), ZoneId.systemDefault()));
        }
    }

    private void addDatesRange() {
        LocalDateTime dateTime = BEGIN_TIME;

        while (dateTime.isBefore(END_TIME)) {
            graphTimelineService.addSingleDateToGraph(dateTime);
            if (dateTime.getHour() < 23) {
                dateTime = dateTime.withHour(dateTime.getHour() + 1);
            } else if (dateTime.getDayOfMonth() < dateTime.with(lastDayOfMonth()).getDayOfMonth()) {
                dateTime = dateTime.withHour(0).withDayOfMonth(dateTime.getDayOfMonth() + 1);
            } else if (dateTime.getMonthValue() < 12) {
                dateTime = dateTime.withHour(0).withDayOfMonth(1).withMonth(dateTime.getMonthValue() + 1);
            } else {
                dateTime = dateTime.withHour(0).withDayOfMonth(1).withMonth(1).withYear(dateTime.getYear() + 1);
            }

        }
    }


}
