package com.epione.timeline.services.impl;

import com.epione.timeline.domain.model.*;
import com.epione.timeline.domain.model.types.SingleDateTimeTree;
import com.epione.timeline.domain.repository.*;
import com.epione.timeline.services.ArrangeTimeNodesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Slf4j
@Service
public class ArrangeTimeNodesServiceImpl implements ArrangeTimeNodesService {

    @Autowired
    private YearRepository yearRepo;

    @Autowired
    private MonthRepository monthRepo;

    @Autowired
    private DayRepository dayRepo;

    @Autowired
    private HourRepository hourRepo;

    @Autowired
    private MinuteRepository minuteRepo;

    @Override
    @Transactional
    public void addToNextTree(Long dateTimeEpoch, SingleDateTimeTree addedTree) {
        Optional<Minute> nextMinuteOptional = minuteRepo.getNextDateTimeMinute(dateTimeEpoch);
        nextMinuteOptional.ifPresent(minute -> {
            log.info("Got next minute: {}", minute);
            SingleDateTimeTree nextTree = minuteRepo.getTreeFromMinuteId(minute.getId());
            log.info("Got next time line tree: {}", nextTree);
            setNextBetweenTrees(addedTree, nextTree);
        });
    }

    @Override
    @Transactional
    public void addToPreviousTree(Long dateTimeEpoch, SingleDateTimeTree addedTree) {
        Optional<Minute> lastMinuteOptional = minuteRepo.getLastDateTimeMinute(dateTimeEpoch);
        lastMinuteOptional.ifPresent(minute -> {
            log.info("Got last minute: {}", minute);
            SingleDateTimeTree previousTree = minuteRepo.getTreeFromMinuteId(minute.getId());
            log.info("Got previous time line tree: {}", previousTree);
            setNextBetweenTrees(previousTree, addedTree);
        });
    }

    private void setNextBetweenTrees(SingleDateTimeTree from, SingleDateTimeTree to) {
        if (from != null && to != null) {
            if (!from.getYear().getUuid().equals(to.getYear().getUuid())) {
                Year fromYear = yearRepo.findById(from.getYear().getId()).get();
                Year toYear = yearRepo.findById(to.getYear().getId()).get();
                fromYear.setNext(toYear);
                yearRepo.save(fromYear);
            }
            if (!from.getMonth().getUuid().equals(to.getMonth().getUuid())) {
                Month fromMonth = monthRepo.findById(from.getMonth().getId()).get();
                Month toMonth = monthRepo.findById(to.getMonth().getId()).get();
                fromMonth.setNext(toMonth);
                monthRepo.save(fromMonth);
            }
            if (!from.getDay().getUuid().equals(to.getDay().getUuid())) {
                Day fromDay = dayRepo.findById(from.getDay().getId()).get();
                Day toDay = dayRepo.findById(to.getDay().getId()).get();
                fromDay.setNext(toDay);
                dayRepo.save(fromDay);
            }
            if (!from.getHour().getUuid().equals(to.getHour().getUuid())) {
                Hour fromHour = hourRepo.findById(from.getHour().getId()).get();
                Hour toHour = hourRepo.findById(to.getHour().getId()).get();
                fromHour.setNext(toHour);
                hourRepo.save(fromHour);
            }
            if (!from.getMinute().getUuid().equals(to.getMinute().getUuid())) {
                Minute fromMinute = minuteRepo.findById(from.getMinute().getId()).get();
                Minute toMinute = minuteRepo.findById(to.getMinute().getId()).get();
                fromMinute.setNext(toMinute);
                minuteRepo.save(fromMinute);
            }
        }
    }
}
