package com.epione.timeline.services;

import com.epione.timeline.domain.model.TimeNode;

import java.time.LocalDateTime;

public interface GraphTimelineService {

    TimeNode addSingleDateToGraph(LocalDateTime dateTime);

    TimeNode getTimeNodeFromDate(LocalDateTime dateTime);

}
