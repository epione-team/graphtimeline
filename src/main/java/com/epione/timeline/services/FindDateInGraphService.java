package com.epione.timeline.services;

import com.epione.timeline.domain.model.TimeNode;

import java.util.Optional;

public interface FindDateInGraphService {

    Optional<TimeNode> findDate(Long dateTimeEpoch);
}
