package com.epione.timeline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GraphTimelineApplication {
    public static void main(String[] args) {
        SpringApplication.run(GraphTimelineApplication.class, args);
    }
}
