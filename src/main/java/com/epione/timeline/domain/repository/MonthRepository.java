package com.epione.timeline.domain.repository;

import com.epione.timeline.domain.model.Month;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface MonthRepository extends Neo4jRepository<Month, Long> {

    Optional<Month> findByValue(int value);

    @Query("MATCH (:Year {value: {yearValue}})-[:CHILD]->(month:Month {value: {monthValue}}) RETURN month")
    Optional<Month> getMonthRelatedToYear(@Param("monthValue") int monthValue, @Param("yearValue") int yearValue);

    @Query("MATCH (:Year {value: {yearValue}})-[:CHILD]->(m:Month) WHERE m.value >= {before} AND m.value<= {after}" +
            " RETURN m ORDER BY m.value;")
    List<Month> findByValueBetweenRelatedToYear(@Param("before") int before, @Param("after") int after,
                                                @Param("yearValue") int yearValue);


    @Query("MATCH (y:Year)-[:CHILD]->(m:Month) WHERE y.value <= {yearValue} AND m.value < {monthValue} " +
            "WITH max(y.value) AS maxYearValue, max(m.value) AS maxMonthValue\n " +
            "WITH maxYearValue, maxMonthValue MATCH (:Year {value:maxYearValue})-[:CHILD]->(lastMonth:Month {value:maxMonthValue}) RETURN lastMonth;")
    Optional<Month> getPreviousMonth(@Param("monthValue") int monthValue, @Param("yearValue") int yearValue);

    @Query("MATCH (y:Year)-[:CHILD]->(m:Month) WHERE y.value >= {yearValue} AND m.value > {monthValue} " +
            "WITH min(y.value) AS minYearValue, min(m.value) AS minMonthValue\n " +
            "WITH minYearValue, minMonthValue MATCH (:Year {value:minYearValue})-[:CHILD]->(nextMonth:Month {value:minMonthValue}) RETURN nextMonth;")
    Optional<Month> getNextMonth(@Param("monthValue") int monthValue, @Param("yearValue") int yearValue);

}
