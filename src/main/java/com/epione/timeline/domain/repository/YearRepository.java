package com.epione.timeline.domain.repository;

import com.epione.timeline.domain.model.Year;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface YearRepository extends Neo4jRepository<Year, Long> {

    Optional<Year> findByValue(int value);

    List<Year> findByValueGreaterThanOrderByValue(int value);

    List<Year> findByValueLessThan(int value);

    List<Year> findByValueGreaterThan(int value);

    List<Year> findByValueBetween(int before, int after);

    @Query("MATCH (y:Year) WHERE y.value < {currentValue} WITH max(y.value) AS maxYearValue\n" +
            "WITH maxYearValue MATCH (maxYear:Year {value:maxYearValue}) RETURN maxYear;")
    Optional<Year> getPreviousYear(@Param("currentValue") int currentValue);

    @Query("MATCH (y:Year) WHERE y.value > {currentValue} WITH min(y.value) AS minYearValue\n" +
            "WITH minYearValue MATCH (minYear:Year {value:minYearValue}) RETURN minYear;")
    Optional<Year> getNextYear(@Param("currentValue") int currentValue);

}
