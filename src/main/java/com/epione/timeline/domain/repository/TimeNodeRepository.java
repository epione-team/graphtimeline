package com.epione.timeline.domain.repository;

import com.epione.timeline.domain.model.TimeNode;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Optional;

//@NoRepositoryBean
public interface TimeNodeRepository extends Neo4jRepository<TimeNode, Long> {

    @Query("match (:TimeNode {value: {parentValue}})-[:CHILD]->(n:TimeNode {value: {childValue}}) return month")
    Optional<TimeNode> getChildByParentValue(@Param("parentValue") Integer yearValue, @Param("monthValue") Integer monthValue);

    List<TimeNode> findByLeafIsTrue();

    Optional<TimeNode> findByFirstIsTrue();

    Optional<TimeNode> findByLastIsTrue();

    List<TimeNode> findByDateTimeAfter(Date otherDate);

    List<TimeNode> findByDateTimeBefore(Date otherDate);

    Optional<TimeNode> findByDateTime(Long dateTime);
}
