package com.epione.timeline.domain.repository;

import com.epione.timeline.domain.model.Minute;
import com.epione.timeline.domain.model.types.SingleDateTimeTree;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface MinuteRepository extends Neo4jRepository<Minute, Long> {

    @Query("MATCH (:Year {value: {yearValue}})-[:CHILD]->(:Month {value: {monthValue}}) " +
            "-[:CHILD]->(:Day {value: {dayValue}})-[:CHILD]->(:Hour {value: {hourValue}})" +
            "-[:CHILD]->(minute:Minute {value: {minuteValue}}) " +
            "RETURN minute;")
    Optional<Minute> findByValueRelatedToYearMonthDayAndHour(
            @Param("minuteValue") int minuteValue,
            @Param("hourValue") int hourValue,
            @Param("dayValue") int dayValue,
            @Param("monthValue") int monthValue,
            @Param("yearValue") int yearValue);

    @Query("MATCH (m:Minute) WHERE m.dateTime < {currentDateTime} WITH max(m.dateTime) AS lastMinuteValue\n" +
            "WITH lastMinuteValue MATCH (lastMinute:Minute {dateTime:lastMinuteValue}) RETURN lastMinute;")
    Optional<Minute> getLastDateTimeMinute(@Param("currentDateTime") Long currentEpochSeconds);

    @Query("MATCH (m:Minute) WHERE m.dateTime > {currentDateTime} WITH min(m.dateTime) AS nextMinuteValue\n" +
            "WITH nextMinuteValue MATCH (nextMinute:Minute {dateTime:nextMinuteValue}) RETURN nextMinute;")
    Optional<Minute> getNextDateTimeMinute(@Param("currentDateTime") Long currentEpochSeconds);

    @Query("MATCH (minute:Minute)<-[:CHILD]-(h:Hour)<-[:CHILD]-(d:Day)<-[:CHILD]-(m:Month)<-[:CHILD]-(y:Year) " +
            "WHERE id(minute) = {minuteId}\n RETURN y AS year, m AS month, d AS day, h AS hour, minute AS minute;")
    SingleDateTimeTree getTreeFromMinuteId(@Param("minuteId") Long minuteId);
}
