package com.epione.timeline.domain.repository;

import com.epione.timeline.domain.model.Hour;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface HourRepository extends Neo4jRepository<Hour, Long> {

    Optional<Hour> findByValue(int value);

    @Query("MATCH (:Year {value: {yearValue}})-[:CHILD]->(:Month {value: {monthValue}}) " +
            "-[:CHILD]->(:Day {value: {dayValue}})-[:CHILD]->(h:Hour {value: {hourValue}}) " +
            "RETURN h;")
    Optional<Hour> findByValueRelatedToYearMonthAndDay(@Param("hourValue") int hourValue,
                                                       @Param("dayValue") int dayValue,
                                                       @Param("monthValue") int monthValue,
                                                       @Param("yearValue") int yearValue);


    @Query("MATCH (:Day {value: {dayValue}})-[:CHILD]->(hour:Hour {value: {hourValue}}) RETURN hour")
    Optional<Hour> getHourRelatedToDay(@Param("dayValue") Integer dayValue, @Param("hourValue") Integer hourValue);

    @Query("MATCH (:Year {value: {yearValue}})-[:CHILD]->(:Month {value: {monthValue}}) " +
            "-[:CHILD]->(:Day {value: {dayValue}})-[:CHILD]->(h:Hour) " +
            "WHERE h.value >= {before} AND h.value<= {after} RETURN h ORDER BY h.value;")
    List<Hour> findByValueBetweenRelatedToYearMonthAndDay(@Param("before") int before, @Param("after") int after,
                                                          @Param("dayValue") int dayValue,
                                                          @Param("monthValue") int monthValue,
                                                          @Param("yearValue") int yearValue);

    @Query("MATCH (y:Year)-[:CHILD]->(m:Month)-[:CHILD]->(d:Day)-[:CHILD]->(h:Hour) " +
            "WHERE y.value <= {yearValue} AND m.value <= {monthValue} AND d.value < {dayValue} " +
            "RETURN h ORDER BY y.value, m.value, d.value, h.value DESC;")
    List<Hour> findPreviousHoursByDayMonthAndYearOrderDesc(@Param("dayValue") int dayValue,
                                                           @Param("monthValue") int monthValue,
                                                           @Param("yearValue") int yearValue);

    @Query("MATCH (y:Year)-[:CHILD]->(m:Month)-[:CHILD]->(d:Day)-[:CHILD]->(h:Hour) " +
            "WHERE y.value >= {yearValue} AND m.value >= {monthValue} AND d.value > {dayValue} " +
            "RETURN h ORDER BY y.value, m.value, d.value, h.value;")
    List<Hour> findNextHoursByDayMonthAndYearOrderDesc(@Param("dayValue") int dayValue,
                                                       @Param("monthValue") int monthValue,
                                                       @Param("yearValue") int yearValue);

    @Query("MATCH (y:Year)-[:CHILD]->(m:Month)-[:CHILD]->(d:Day)-[:CHILD]->(h:Hour)" +
            "WHERE y.value <= {yearValue} AND m.value <= {monthValue} AND d.value <= {dayValue} AND h.value < {hourValue} " +
            "WITH max(y.value) AS maxYearValue, max(m.value) AS maxMonthValue, max(d.value) AS maxDayValue, max(h.value) AS maxHourValue \n " +
            "WITH maxYearValue, maxMonthValue, maxDayValue, maxHourValue " +
            "MATCH (:Year {value:maxYearValue})-[:CHILD]->(:Month {value:maxMonthValue})" +
            "-[:CHILD]->(:Day {value:maxDayValue})-[:CHILD]->(lastHour:Hour {value:maxHourValue}) RETURN lastHour;")
    Optional<Hour> getPreviousHour(@Param("hourValue") int hourValue, @Param("dayValue") int dayValue,
                                   @Param("monthValue") int monthValue, @Param("yearValue") int yearValue);

    @Query("MATCH (y:Year)-[:CHILD]->(m:Month)-[:CHILD]->(d:Day)-[:CHILD]->(h:Hour)" +
            "WHERE y.value >= {yearValue} AND m.value >= {monthValue} AND d.value >= {dayValue} AND h.value > {hourValue} " +
            "WITH min(y.value) AS minYearValue, min(m.value) AS minMonthValue, min(d.value) AS minDayValue, min(h.value) AS minHourValue \n " +
            "WITH minYearValue, minMonthValue, minDayValue, minHourValue " +
            "MATCH (:Year {value:minYearValue})-[:CHILD]->(:Month {value:minMonthValue})" +
            "-[:CHILD]->(:Day {value:minDayValue})-[:CHILD]->(nextHour:Hour {value:minHourValue}) RETURN nextHour;")
    Optional<Hour> getNextHour(@Param("hourValue") int hourValue, @Param("dayValue") int dayValue,
                               @Param("monthValue") int monthValue, @Param("yearValue") int yearValue);

}
