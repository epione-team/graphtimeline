package com.epione.timeline.domain.repository;

import com.epione.timeline.domain.model.Day;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface DayRepository extends Neo4jRepository<Day, Long> {

    Optional<Day> findByValue(int value);

    @Query("MATCH (:Year {value: {yearValue}})-[:CHILD]->(:Month {value: {monthValue}})-[:CHILD]->" +
            "(day:Day {value: {dayValue}}) RETURN day")
    Optional<Day> findByValueRelatedToYearAndMonth(@Param("dayValue") int dayValue, @Param("monthValue") int monthValue,
                                                   @Param("yearValue") int yearValue);

    @Query("MATCH (:Month {value: {monthValue}})-[:CHILD]->(day:Day {value: {dayValue}}) RETURN day")
    Optional<Day> getDayRelatedToMonth(@Param("monthValue") Integer monthValue, @Param("dayValue") Integer dayValue);

    @Query("MATCH (:Year {value: {yearValue}})-[:CHILD]->(:Month {value: {monthValue}})-[:CHILD]->(d:Day)" +
            "WHERE d.value >= {before} AND d.value<= {after} RETURN d ORDER BY d.value;")
    List<Day> findByValueBetweenRelatedToYearAndMonth(@Param("before") int before, @Param("after") int after,
                                                      @Param("monthValue") int monthValue, @Param("yearValue") int yearValue);

    @Query("MATCH (y:Year)-[:CHILD]->(m:Month)-[:CHILD]->(d:Day) " +
            " WHERE y.value <= {yearValue} AND m.value <= {monthValue} AND d.value < {dayValue} " +
            "RETURN d ORDER BY d.value DESC;")
    List<Day> getPreviousDays(@Param("dayValue") int dayValue, @Param("monthValue") int monthValue,
                              @Param("yearValue") int yearValue);

    @Query("MATCH (y:Year)-[:CHILD]->(m:Month)-[:CHILD]->(d:Day) WHERE y.value <= {yearValue} AND m.value <= {monthValue} AND d.value < {dayValue} " +
            "WITH max(y.value) AS maxYearValue, max(m.value) AS maxMonthValue, max(d.value) AS maxDayValue \n " +
            "WITH maxYearValue, maxMonthValue, maxDayValue " +
            "MATCH (:Year {value:maxYearValue})-[:CHILD]->(:Month {value:maxMonthValue})-[:CHILD]->(lastDay:Day {value:maxDayValue}) RETURN lastDay;")
    Optional<Day> getPreviousDay(@Param("dayValue") int dayValue, @Param("monthValue") int monthValue, @Param("yearValue") int yearValue);

    @Query("MATCH (y:Year)-[:CHILD]->(m:Month)-[:CHILD]->(d:Day) WHERE y.value >= {yearValue} AND m.value >= {monthValue} AND d.value > {dayValue} " +
            "WITH min(y.value) AS minYearValue, min(m.value) AS minMonthValue, min(d.value) AS minDayValue \n " +
            "WITH minYearValue, minMonthValue, minDayValue " +
            "MATCH (:Year {value:minYearValue})-[:CHILD]->(:Month {value:minMonthValue})-[:CHILD]->(nextDay:Day {value:minDayValue}) RETURN nextDay;")
    Optional<Day> getNextDay(@Param("dayValue") int dayValue, @Param("monthValue") int monthValue, @Param("yearValue") int yearValue);
}
