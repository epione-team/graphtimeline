package com.epione.timeline.domain.model;

import lombok.*;
import org.neo4j.ogm.annotation.NodeEntity;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Setter
@Getter
@NodeEntity(label = "Month")
public class Month extends TimeNode {
    @NonNull
    private Integer value;

}
