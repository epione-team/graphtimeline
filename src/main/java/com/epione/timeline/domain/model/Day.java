package com.epione.timeline.domain.model;

import lombok.*;
import org.neo4j.ogm.annotation.NodeEntity;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NodeEntity(label = "Day")
public class Day extends TimeNode {
    @NonNull
    protected Integer value;

}
