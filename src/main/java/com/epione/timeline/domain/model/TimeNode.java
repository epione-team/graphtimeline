package com.epione.timeline.domain.model;

import lombok.*;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude = "id")
@ToString(callSuper = true)
@Setter
@Getter
@NodeEntity
public abstract class TimeNode {
    @Id
    @GeneratedValue
    private Long id;

    protected String uuid;

    @Relationship(type = "CHILD")
    protected Set<TimeNode> children = new HashSet<>();

    @Relationship(type = "NEXT")
    protected TimeNode next;

    protected Boolean last;

    protected Boolean first;

    protected Boolean leaf;

    protected Long dateTime;

    public void addChild(TimeNode timeNode) {
        children.add(timeNode);
    }
}
