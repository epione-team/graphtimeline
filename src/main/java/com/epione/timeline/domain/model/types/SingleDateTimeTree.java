package com.epione.timeline.domain.model.types;

import com.epione.timeline.domain.model.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.springframework.data.neo4j.annotation.QueryResult;

@Data
@NoArgsConstructor
@QueryResult
public class SingleDateTimeTree {

    @NonNull
    private Minute minute;
    @NonNull
    private Hour hour;
    @NonNull
    private Day day;
    @NonNull
    private Month month;
    @NonNull
    private Year year;

    public SingleDateTimeTree(Year year, Month month, Day day, Hour hour, Minute minute) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.minute = minute;

        this.year.addChild(this.month);
        this.month.addChild(this.day);
        this.day.addChild(this.hour);
        this.hour.addChild(this.minute);
        this.minute.setLeaf(true);
    }
}
